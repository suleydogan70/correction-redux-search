import { fromJS } from 'immutable'

import initialState from './initial-state'
import actionsType from '../actions/actions-type'

const searchLastEvents = (state, action) => (
  fromJS(state)
    .setIn(['data'], action.data)
    .toJS()
)

const updateSearchField = (state, action) => (
  fromJS(state)
    .setIn(['searchField'], action.searchField)
    .toJS()
)

const search = (state = initialState, action) => {
  switch (action.type) {
    case actionsType.SEARCH_LAST_EVENTS:
      return searchLastEvents(state, action)
    case actionsType.UPDATE_SEARCHFIELD:
      return updateSearchField(state, action)
    default:
      return state
  }
}

export default search
