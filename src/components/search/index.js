import React from 'react'
import { connect } from 'react-redux'

import Results from './components/results'
import InputQuery from './components/input-query'

const Search = ({ search }) => (
  <div>
    <InputQuery />
    <Results data={search.data} />
  </div>
)

export default connect(state => state)(Search)
