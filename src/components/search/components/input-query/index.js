import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getSearchData, updateSearchField } from '../../actions'

class SearchQuery extends Component {
  constructor(props) {
    super(props)

    const { dispatch } = this.props

    this.dispatch = dispatch
    this.timer = 1
  }

  handleChange(e) {
    e.persist()

    clearTimeout(this.timer)

    this.timer = setTimeout(() => {
      this.dispatch(updateSearchField(e.target.value))
    }, 1000)
  }

  handleSubmit(e, searchField) {
    e.preventDefault()
    getSearchData(searchField)
  }

  render() {
    const { search } = this.props
    const { searchField } = search

    return (
      <form>
        <input type="text" className="orm-control mr-sm-2" name="texte" onChange={e => this.handleChange(e)} />
        <input type="submit" className="btn" value="Submit" onClick={e => this.handleSubmit(e, searchField)} />
      </form>
    )
  }
}

export default connect(state => state)(SearchQuery)
